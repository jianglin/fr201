<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<title>Spending Tracker 201</title>
<!-- Bootstrap -->
<link href="css/custom.css" rel="stylesheet">
<link href="css/toaster.css" rel="stylesheet">
<link href="css/sweetalert.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
<link href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css" rel="stylesheet">

<style>
a {
	color: orange;
}
</style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]><link href= "css/bootstrap-theme.css"rel= "stylesheet" >

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body ng-cloak="">
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="row">
				<div class="navbar-header col-md-8">
					<button type="button" class="navbar-toggle" toggle="collapse"
						target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" rel="home"
						title="Spanding Tracker 201">Spending Tracker 201 </a>
				</div>
				
				<div class="navbar-header col-md-2">
					<a class="navbar-brand" rel="home"
						title="Help" ng-click="help()"
						>Help</a>
				</div>
				<div class="navbar-header col-md-1">
					<a class="navbar-brand" rel="home"
						title="Logout" ng-click="logout()"
						>Logout</a>
				</div>
			</div>
		</div>
	</div>
	<div>
		<div class="container" style="margin-top: 20px;">

			<div data-ng-view="" id="ng-view" class="slide-animation"></div>

		</div>
	</div>
</body>
<toaster-container toaster-options="{'time-out': 3000}"></toaster-container>

<!-- JQuery -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>


<script src="js/angular.js"></script> 
<script src="js/angular-sanitize.js"></script> 
<script src="js/angular-animate.js"></script>  
<script src="js/angular-datatables.min.js"></script>
<script src="js/ui-bootstrap-tpls-2.1.1.js"></script>    
<script src="js/angular-route.min.js"></script>

  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-aria.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular-messages.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.js"></script>

<script src="js/toaster.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="app/app.js"></script>
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
<script src="app/authCtrl.js"></script>
<script src="app/testCtrl.js"></script>
<script src="app/tracCtrl.js"></script>

    
    
</html>

