<?php

class DbHandler {

    private $conn;

    function __construct() {
        require_once 'dbConnect.php';
        // opening db connection
        $db = new dbConnect();
        $this->conn = $db->connect();
    }
    /**
     * Fetching single record
     */
    public function getOneRecord($query) {
        $r = $this->conn->query($query.' LIMIT 1') or die($this->conn->error.__LINE__);
         return $result = $r->fetch_assoc(); 
    }
    /**
     * Fetching all records
     */
    public function getAllRecords($query) {
    	$r = $this->conn->query($query) or die($this->conn->error.__LINE__);
		$results_array = [];
		while ($row = $r->fetch_assoc()) {
			array_push($results_array, $row);
		}
		return $results_array;
    }

    /**
     * Creating new record
     */
    public function insertIntoTable($obj, $column_names, $table_name) {
        
        $c = (array) $obj;
        $keys = array_keys($c);
        $columns = '';
        $values = '';
        foreach($column_names as $desired_key){ // Check the obj received. If blank insert blank into the array.
           if(!in_array($desired_key, $keys)) {
                $$desired_key = '';
            }else{
                $$desired_key = $c[$desired_key];
            }
            $columns = $columns.$desired_key.',';
            $values = $values."'".$$desired_key."',";
        }
        $query = "INSERT INTO ".$table_name."(".trim($columns,',').") VALUES(".trim($values,',').")";
        $r = $this->conn->query($query) or die($this->conn->error.__LINE__);

        if ($r) {
            $new_row_id = $this->conn->insert_id;
            return $new_row_id;
            } else {
            return NULL;
        }
    }
    /**
     * Update record
     */
    public function updateTableByID($obj, $column_names, $table_name, $key, $id) {
    
    	$c = (array) $obj;
    	$keys = array_keys($c);
    	$columns = '';
    	$values = '';
    	$updates = '';
    	foreach($column_names as $desired_key){ // Check the obj received. If blank insert blank into the array.
    		if(!in_array($desired_key, $keys)) {
    			$$desired_key = '';
    		}else{
    			$$desired_key = $c[$desired_key];
    			$updates = $updates.$desired_key.'='."'".$$desired_key."',";
    		}
    	}
    	$query = "UPDATE ".$table_name." SET ".trim($updates,',')." WHERE ".$key."=".$id;
    	$r = $this->conn->query($query) or die($this->conn->error.__LINE__);
	    return $r;
    }    
    /**
     * Delete record
     */
    public function deleteRecordByID($table_name, $key, $id) {
    	$query = "DELETE FROM ".$table_name." WHERE ".$key."=".$id;
    	$r = $this->conn->query($query) or die($this->conn->error.__LINE__);
    	return $r;
    }
    public function isUserExists($phone, $email){
    	$query = "SELECT 1 FROM Users WHERE phone='$phone' or email='$email'";
    	return $this->getOneRecord($query);
    }
    public function getUserID($selected_uid){
    	$query = "SELECT uid FROM Users WHERE uid='$selected_uid'";
    	return $this->getOneRecord($query);
    }
    public function getUserInfo($email){
    	$query = "SELECT uid,first_name,password,email,created,default_group FROM Users WHERE phone='$email' or email='$email'";
    	return $this->getOneRecord($query);
    }
    public function getCurrentGroup($groupTable, $gid){
    	$query = "SELECT group_name FROM $groupTable WHERE gid='$gid'";
    	return $this->getOneRecord($query);
    }

    public function getMonthlySummary($groupTable, $trackingTable, $participantsTable, $selected_gid, $summaryMY){
    	$query = "SELECT h.hid, h.description, h.amount, c.cid, c.category_name, g.gid, g.group_name, h.created, DATE(h.transaction_time) AS transaction_time, h.creator, u.first_name FROM $trackingTable AS h INNER JOIN Categories AS c ON h.cid = c.cid 
																						INNER JOIN $groupTable AS g ON h.gid = g.gid
																						INNER JOIN $participantsTable AS t ON t.hid = h.hid
																						INNER JOIN Users AS u ON h.creator = u.uid
																						WHERE g.gid = '$selected_gid' 
																						AND (MONTH(h.transaction_time) = '$summaryMY[0]')																		
																						AND (YEAR(h.transaction_time) = '$summaryMY[1]')
																						GROUP BY h.hid";
    	return $this->getAllRecords($query);
    }
    public function getSummary($groupTable, $trackingTable, $participantsTable, $selected_gid){
    	$query = "SELECT h.hid, h.description, h.amount, c.cid, c.category_name, g.gid, g.group_name, h.created, DATE(h.transaction_time) AS transaction_time, h.creator, u.first_name FROM $trackingTable AS h INNER JOIN Categories AS c ON h.cid = c.cid
																				    	INNER JOIN $groupTable AS g ON h.gid = g.gid
																				    	INNER JOIN $participantsTable AS t ON t.hid = h.hid
																				    	INNER JOIN Users AS u ON h.creator = u.uid
																				    	WHERE g.gid = '$selected_gid'
																				    	GROUP BY h.hid";
    	return $this->getAllRecords($query);
    }
    public function getTransactionDetails($trackingTable , $groupTable, $participantsTable, $selected_gid, $startDate, $endDate){
    	$query = "SELECT h.hid, h.description, h.amount, c.cid, c.category_name, g.gid, g.group_name, h.created, DATE(h.transaction_time) AS transaction_time, h.creator, u.first_name FROM $trackingTable AS h INNER JOIN Categories AS c ON h.cid = c.cid 
																						INNER JOIN $groupTable AS g ON h.gid = g.gid
																						INNER JOIN $participantsTable AS t ON t.hid = h.hid
																						INNER JOIN Users AS u ON h.creator = u.uid
																						WHERE g.gid = '$selected_gid' 
																						AND (h.transaction_time BETWEEN '$startDate' AND '$endDate' + INTERVAL 1 MONTH - INTERVAL 1 DAY)
																						GROUP BY h.hid";
    	return $this->getAllRecords($query);
    }
    public function getPaticipants($participantsTable, $trackingTable,$hid){
    	$query = "SELECT u.uid, u.first_name FROM Users AS u INNER JOIN $participantsTable  AS t ON u.uid = t.uid INNER JOIN $trackingTable AS h ON h.hid = t.hid WHERE h.hid = '$hid'";
    	return $this->getAllRecords($query);
    }
    public function loadCategories() {
    	$query = "SELECT cid, category_name FROM Categories";
    	return $this->getAllRecords ($query);
    }
    public function loadGroups($membershipTable,$groupTable,$uid){
    	$query = "SELECT m.role, g.gid, g.group_name FROM $membershipTable AS m INNER JOIN Users AS u ON m.uid = u.uid
    			INNER JOIN $groupTable AS g ON m.gid = g.gid
    			WHERE u.uid = '$uid'";
    	return $this->getAllRecords ($query);
    
    
    
    }
    public function loadUsers($membershipTable,$groupTable,$currentGroupID){
    	$query =  "SELECT u.uid, u.first_name, m.role, g.gid, g.group_name, true AS isSelected FROM $membershipTable AS m INNER JOIN Users AS u ON m.uid = u.uid
    			INNER JOIN $groupTable AS g ON m.gid = g.gid
    			WHERE g.gid = '$currentGroupID'" ;
    	return $this->getAllRecords($query);
    
    }
public function getSession(){
    if (!isset($_SESSION)) {
        session_start();
    }
    $sess = array();
    if(isset($_SESSION['uid']))
    {
        $sess["uid"] = $_SESSION['uid'];
        $sess["name"] = $_SESSION['name'];
        $sess["email"] = $_SESSION['email'];
        $sess["gid"] = $_SESSION['gid'];
    }
    else
    {
        $sess["uid"] = '';
        $sess["name"] = 'Guest';
        $sess["email"] = '';
        $sess["gid"] = '';
    }
    return $sess;
}
public function updateGroupSessionValue($value){
	if (!isset($_SESSION)) {
		session_start();
	}
	$sess = array();
	if(isset($_SESSION['uid']))
	{
		$_SESSION['gid'] = $value;
	}
	else
	{
		$sess["uid"] = '';
		$sess["name"] = 'Guest';
		$sess["email"] = '';
		$sess["gid"] = '';
	}
	return $sess;
}
public function destroySession(){
    if (!isset($_SESSION)) {
    session_start();
    }
    if(isSet($_SESSION['uid']))
    {
        unset($_SESSION['uid']);
        unset($_SESSION['name']);
        unset($_SESSION['email']);
        unset($_SESSION['gid']);
        $info='info';
        if(isSet($_COOKIE[$info]))
        {
            setcookie ($info, '', time() - $cookie_time);
        }
        $msg="Logged Out Successfully...";
    }
    else
    {
        $msg = "Not logged in...";
    }
    return $msg;
}
 
}

?>
