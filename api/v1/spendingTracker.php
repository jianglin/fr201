<?php

$app->post ( '/autoload', function () use ($app) {

	$r = json_decode ( $app->request->getBody () );
	$selected = $r->selected;
	$groupsOrEvents = $selected->groupsOrEvents;
	$db = new DbHandler ();
	$session = $db->getSession ();
	$uid = $session ['uid'];
	$gid = $session ['gid'];
	$name = $session ['name'];
	$email = $session ["email"];
	$response = array();
	$currentUser = ['uid' => $uid, 'name' => $name, 'email' => $email];
	$availableCategories = $db->loadCategories();
	/*
	$availableStartDate = mktime ( 0, 0, 0, 1, 1, 1980 );
	$availableEndDate = mktime ( 0, 0, 0, 1, 1, 2099 );
	$availableDates = [
			$availableStartDate,
			$availableEndDate
	];
	*/
	if ($groupsOrEvents === "groups") {
		$groupTable = 'Groups';
		$membershipTable = 'Membership';
	
	
	} else {
		$groupTable = 'Events';
		$membershipTable = 'Events_Membership';
	
	}
	$availableGroups = $db->loadGroups($membershipTable,$groupTable,$uid);
	
	if ($groupsOrEvents === "groups") {
		$result = $db->getCurrentGroup( $groupTable,$gid );
		$currentGroup = ['groupid' => $gid, 'groupname' => $result['group_name']];
	} else {
		$eventID =  $availableGroups[0]['gid'];
		$eventName = $availableGroups[0]['group_name'];
		$currentGroup = ['groupid' => $eventID, 'groupname' => $eventName];
	}
	$currentGroupID = $currentGroup['groupid'];
	date_default_timezone_set('America/New_York');
	$currentDate = date ( "Y-m" );
	$currentMonthArray = explode("-", $currentDate);
	$currentMY = [ 
			ltrim($currentMonthArray[1], '0'),
			$currentMonthArray[0] 
	];
	
	
	
	$availableUsers = $db->loadUsers($membershipTable,$groupTable,$currentGroupID);
	
	
	$participants = [];
	foreach ( $availableUsers as $availableUser ) {
		array_push($participants, $availableUser ['uid']);
	}
	
	$response ['currentGroup'] = $currentGroup;
	$response ['currentUser'] = $currentUser;
	$response ['currentDate'] = $currentDate;
	$response ['currentMonthYear'] = $currentMY;
	$response ['availableGroups'] = $availableGroups;
	$response ['availableUsers'] = $availableUsers;
	//$response ['availableDates'] = $availableDates;
	$response ['availableCategories'] = $availableCategories;
	$response ['participantsSelection'] = $participants; 
	$response ["status"] = "success";
	$response ["message"] = "Infomation loaded for user " . $name;
	echoResponse ( 200, $response );
} );

$app->post ( '/loadSummary', function () use ($app) {
	$r = json_decode ( $app->request->getBody () );
	$selected = $r->selected;
	$groupsOrEvents = $selected->groupsOrEvents;
	$selected_gid = $selected->gid;
	// $selected_uid = $selected->uid;
	if ($groupsOrEvents === "groups"){
	
		$summary_month = $selected->summaryMonth;
		
		$year_month = explode ( "-", $summary_month );
		$summaryMY = [ 
				ltrim ( $year_month [1], '0' ),
				$year_month [0] 
		];
		$groupTable = 'Groups';
		$memberShipTable = 'Membership';
		$trackingTable = 'Tracking';
		$participantsTable = 'Transaction_Participants';
	} else {
		$groupTable = 'Events';
		$memberShipTable = 'Events_Membership';
		$trackingTable = 'Events_Tracking';
		$participantsTable = 'Events_Transaction_Participants';
	}
	$current_month = $selected->currentMonth;
	$year_month = explode ( "-", $current_month );
	$currentMY = [
			ltrim ( $year_month [1], '0' ),
			$year_month [0]
	];
	$response = array ();
	$db = new DbHandler ();
	$availableUsers = $db->loadUsers($memberShipTable, $groupTable, $selected_gid);

	$summary_table = [ ];
	$summary_paid = [ ];
	$name_table = [ ];
	$spending = [ ];
	foreach ( $availableUsers as $user ) {
		$summary_table [$user ['uid']] = [ ];
		$summary_paid [$user ['uid']] = floatval ( 0.0 );
		$spending [$user ['uid']] = floatval ( 0.0 );
		$name_table [$user ['uid']] = $user ['first_name'];
		foreach ( $availableUsers as $related_user ) {
			$summary_table [$user ['uid']] [$related_user ['uid']] = floatval ( 0.0 );
		}
	}
	if ($groupsOrEvents === "groups") {
		$records = $db->getMonthlySummary ( $groupTable, $trackingTable, $participantsTable, $selected_gid, $summaryMY);
	} else {
		$records = $db->getSummary ( $groupTable, $trackingTable, $participantsTable, $selected_gid);
	}
	if ($records != NULL) {
		//collect spending info and store to $summary_table
		
		foreach ( $records as $record ) {
			$hid = $record ['hid'];
			$users = $db->getPaticipants (  $participantsTable, $trackingTable,$hid );
			foreach ( $users as $related_user ) {
				$average = floatval ( $record ['amount'] / ( float ) count ( $users ) );
				$summary_table [$record ['creator']] [$related_user ['uid']] += $average;
				$spending [$related_user ['uid']] += $average;
			}
			$summary_paid [$record ['creator']] += $record ['amount'];
		}
		//calculate balance between any two related users
		foreach ( $availableUsers as $user ) {
			foreach ( $availableUsers as $related_user ) {
				if ($user ['uid'] != $related_user ['uid']) {
					$max = max ( $summary_table [$user ['uid']] [$related_user ['uid']], $summary_table [$related_user ['uid']] [$user ['uid']] );
					$summary_table [$user ['uid']] [$related_user ['uid']] -= $max;
					$summary_table [$related_user ['uid']] [$user ['uid']] -= $max;
				}
			}
		}
		//adjust $summary_table for display
		foreach ( $availableUsers as $user ) {
			foreach ( $availableUsers as $related_user ) {
				if ($summary_table [$user ['uid']] [$related_user ['uid']] < 0) {
					$summary_table [$related_user ['uid']] [$user ['uid']] = - $summary_table [$user ['uid']] [$related_user ['uid']];
				} else if ($user ['uid'] == $related_user ['uid']) {
					$summary_table [$related_user ['uid']] [$user ['uid']] = $spending [$user ['uid']];
				}
			}
		}
		$response ['status'] = "success";
		$response ['message'] = 'Monthly Summary loaded';
	} else {
		$response ['status'] = "warning";
		$response ['message'] = 'No record found.';
	}
	
	$response ['summaryTable'] = $summary_table;
	$response ['nameTable'] = $name_table;
	$response ['summaryPaid'] = $summary_paid;
	
	echoResponse ( 200, $response );
} );
	
	
$app->post ( '/retriveTrackingData', function () use ($app) {
	$r = json_decode ( $app->request->getBody () );
	$response = array ();
	$db = new DbHandler ();
	$session = $db->getSession ();
	$uid = $session ['uid'];
	$gid = $session ['gid'];
	$name = $session ['name'];
	$email = $session ["email"];
	$selected = $r->selected;
	$groupsOrEvents = $selected->groupsOrEvents;
	$selected_gid = $selected->gid;
	if ($groupsOrEvents === "groups") {
		$groupTable = 'Groups';
		$membershipTable = 'Membership';
		$trackingTable = 'Tracking';
		$participantsTable = 'Transaction_Participants';
		if (isset ( $selected->startMonth ) && isset ( $selected->endMonth )) {
			$selected_start_month = $selected->startMonth;
			$selected_end_month = $selected->endMonth;
		} else if (isset ( $selected->startMonth )) {
			$selected_start_month = $selected_end_month = $selected->startMonth;
		} else if (isset ( $months->endMonth )) {
			$selected_start_month = $selected_end_month = $selected->endMonth;
		} else {
			$selected_start_month = $selected_end_month = date ( "Y-m" );
		}
		$year_month = explode("-", $selected_start_month);
		$startMY = [
				ltrim($year_month[1], '0'),
				$year_month[0]
		];
		$year_month = explode("-", $selected_end_month);
		$endMY = [
				ltrim($year_month[1], '0'),
				$year_month[0]
		];
		$response ['selectedStartMonth'] = $selected_start_month;
		$response ['selectedEndMonth'] = $selected_end_month;
	} else {
		$groupTable = 'Events';
		$membershipTable = 'Events_Membership';
		$trackingTable = 'Events_Tracking';
		$participantsTable = 'Events_Transaction_Participants';
		$startMY = ['01', '1980'];
		$endMY = ['01', '2099'];
		$response ['selectedStartMonth'] = '1980-01';
		$response ['selectedEndMonth'] = '2099-01';
	}
	
	$result = $db->getCurrentGroup ( $groupTable ,$selected_gid );
	$currentGroup = ['groupid' => $selected_gid, 'groupname' =>  $result['group_name']];
	
	if ($groupsOrEvents === "groups"){
		if ($selected_gid != $gid) {
			$db->updateGroupSessionValue ( $selected_gid );
			$selected_uid = $uid;
		} else {
			$selected_uid = $selected->uid;
		}
	} else {
		//TODO:if selected uid does not exist in current, selected_uid should set to current user id 
		$selected_uid = $selected->uid;
	}

	$currentMonthYear = $selected->currentMonthYear;

	$selected_user = $db->getUserID ($selected_uid );
	
	if ($selected_user != NULL) {
		$response ['selectedUID'] = $selected_uid;
		$response ['selectedGID'] = $selected_gid;
		$availableUsers = $db->loadUsers($membershipTable,$groupTable,$selected_gid);
		$participants = [];
		foreach ( $availableUsers as $availableUser ) {
			array_push ( $participants, $availableUser ['uid'] );
		}
		$response ['availableUsers'] = $availableUsers;
		$response ['participantsSelection'] = $participants;
		$response ['currentGroup'] = $currentGroup;
		$startDate = $startMY[1] . '-' . $startMY[0] . '-01';
		$endDate = $endMY[1] . '-' . $endMY[0] . '-01';
		$records = $db->getTransactionDetails ( $trackingTable, $groupTable, $participantsTable, $selected_gid, $startDate, $endDate );
		$tracking_data = [];
		foreach ( $records as $record ) {
			$hid = $record ['hid'];
			$users = $db->getPaticipants ( $participantsTable, $trackingTable, $hid);
			$users_string = '';
			$has_participated = false;
			foreach ( $users as $user ) {
				$users_string .= $user ['first_name'] . ". ";
				if ($user ['uid'] == $selected_uid) {
					$has_participated = true;
				}
			}
			$user_lists = [ 
					'user_lists' => $users_string,
					'has_participated' => $has_participated 
			];	
			array_push ( $tracking_data, array_merge ( $record, $user_lists ) );
		}
		$response ['trackingData'] = $tracking_data;
		$response ['status'] = "success";
		$response ['message'] = 'Successfully load Tracking Data';
	} else {
		$response ['status'] = "error";
		$response ['message'] = 'User does not exists.';
	}

	echoResponse ( 200, $response );
} );


$app->post ( '/addTransaction', function () use ($app) {
	$r = json_decode ( $app->request->getBody () );
	$selected = $r->selected;
	$trans = $r->trans;
	$cid = $trans->category;
	$description = $trans->description;
	$amount = $trans->amount;
	$date = $trans->date;
	$participants = $trans->participants;
	$groupsOrEvents = $selected->groupsOrEvents;
	$creator = $selected->uid;
	$gid = $selected->gid;
	// TODO: validate input.
	$response = array ();
	$db = new DbHandler ();
	date_default_timezone_set('UTC');
    $local_date = new DateTime($date);
    $local_date->setTimeZone(new DateTimeZone('America/New_York'));
    $local_date_string = $local_date->format("Y-m-d h:i:s");
    date_default_timezone_set('America/New_York'); 
	$values = [ 
			'gid' => $gid,
			'cid' => $cid,
			'description' => $description,
			'amount' => $amount,
			'creator' => $creator ,
			'transaction_time' => $local_date_string
	];
	$column_names = [ 
			'gid',
			'cid',
			'description',
			'amount',
			'creator',
			'transaction_time'
	];
	if ($groupsOrEvents === "groups"){
		$tabble_name = 'Tracking';
	} else {
		$tabble_name = 'Events_Tracking';
	}
	$hid = $db->insertIntoTable ( $values, $column_names, $tabble_name );
	$response ['status'] = "success";
	$response ['message'] = 'Transaction added Successfully.';
	if ($hid != NULL) {
		$column_names = [ 
				'hid',
				'uid' 
		];
		if ($groupsOrEvents === "groups"){
			$tabble_name = 'Transaction_Participants';
		} else {
			$tabble_name = 'Events_Transaction_Participants';
		}
		$result = 0;
		foreach ( $participants as $participant ) {
			$values = [ 
					'hid' => $hid,
					'uid' => $participant 
			];
			$result = $db->insertIntoTable ( $values, $column_names, $tabble_name );
			if ($result == NULL) {
				$response ['status'] = "error";
				$response ['message'] = "Error on inserting hid='$hid', uid='$creator' to '$tabble_name'";
				break;
			}
		}
	} else {
		$response ['status'] = "error";
		$response ['message'] = "Error on inserting values to '$tabble_name'";
	}
	echoResponse ( 200, $response );
} );

/*Not working. TODO: check selected participants*/
$app->post ( '/updateTransaction', function () use ($app) {
	$r = json_decode ( $app->request->getBody () );
	$response ['status'] = "success";
	$response ['message'] = "value " . $app->request->getBody ();
	$trans = $r->trans;
	$cid = $trans->category;
	$description = $trans->description;
	$amount = $trans->amount;
	$date = $trans->date;
	$participants = $trans->participants;
	$hid = $r->hid;
	$creator = $r->creator;
	// todo validate input.
	$response = array ();
	$db = new DbHandler ();
	$session = $db->getSession ();
	$uid = $session ['uid'];
	$gid = $session ['gid'];
	$values = [ 
			'gid' => $gid,
			'cid' => $cid,
			'description' => $description,
			'amount' => $amount,
			'creator' => $creator,
			'created' => date ( "Y-m-d H:i:s" ),
			'transaction_time' => $date
	];
	$column_names = [ 
			'gid',
			'cid',
			'description',
			'amount',
			'creator',
			'created',
			'transaction_time'
	];
	$tabble_name = 'Tracking';
	$key = 'hid';
	$result = $db->updateTableByID ( $values, $column_names, $tabble_name, $key, $hid );
	$response ['status'] = "success";
	$response ['message'] = 'Successfully Updated Transaction (' . $hid . ')';
	if ($result) {
		$table_name = 'Transaction_Participants';
		$key = 'hid';
		$value = $hid;
		$result = $db->deleteRecordByID ( $table_name, $key, $value );
		if ($result) {
			$column_names = [ 
					'hid',
					'uid' 
			];
			$tabble_name = 'Transaction_Participants';
			foreach ( $participants as $participant ) {
				$values = [ 
						'hid' => $hid,
						'uid' => $participant 
				];
				$result = $db->insertIntoTable ( $values, $column_names, $tabble_name );
				if ($result == NULL) {
					$response ['status'] = "error";
					$response ['message'] = "Error on inserting hid='$hid', uid='$participant' to '$tabble_name'";
					break;
				}
			}
		} else {
			$response ['status'] = "error";
			$response ['message'] = "Error on deleting record ('$hid') from '$tabble_name'";
		}
	} else {
		$response ['status'] = "error";
		$response ['message'] = "Error on inserting values to '$tabble_name'";
	}
	echoResponse ( 200, $response );
} );
$app->post ( '/removeTransaction', function () use ($app) {
	$r = json_decode ( $app->request->getBody () );
	$groupsOrEvents = $r->groupsOrEvents;
	$hid = $r->hid;
	$response = array ();
	$db = new DbHandler ();
	$session = $db->getSession ();
	if ($groupsOrEvents === "groups"){
			$table_name = 'Transaction_Participants';
	} else {
			$table_name = 'Events_Transaction_Participants';
	}

	$key = 'hid';
	$value = $hid;
	
	$result = $db->deleteRecordByID ( $table_name, $key, $value );
	$response['loadTest'] = $table_name ;
	if ($result) {
		if ($groupsOrEvents === "groups"){
			$table_name = 'Tracking';
		} else {
			$table_name = 'Events_Tracking';
		}
		$result = $db->deleteRecordByID ( $table_name, $key, $value );
		if ($result) {
			$response ['status'] = "success";
			$response ['message'] = "Successfully removed transaction ('$hid') and participants.";
		} else {
			$response ['status'] = "error";
			$response ['message'] = "Error on removing '$hid' from '$table_name'";
		}
	} else {
		$response ['status'] = "error";
		$response ['message'] = "Error on removing '$hid' from '$table_name'";
	}
	echoResponse ( 200, $response );
} );
?>
