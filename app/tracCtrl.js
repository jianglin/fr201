app.controller('tracCtrl', function($scope, $rootScope, $routeParams, $location, $http, Data, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.selected = {};
    $scope.selected.groupsOrEvents = "groups";
    $scope.trans = {};


    $scope.autoload =function(selected) {
    	Data.post('autoload', {
    		selected: selected
        }).then(function(response) {
            Data.toast(response);
            if (response.status == "success") {
				$scope.currentGroup = response.currentGroup;
                $scope.selected.currentMonthYear = response.currentMonthYear;
                $scope.availableGroups = response.availableGroups;
                $scope.currentUser = response.currentUser;
                $rootScope.loginName = response.currentUser.name;
                $rootScope.loginUID = response.currentUser.uid;
                $scope.availableUsers = response.availableUsers;
                $scope.availableDates = response.availableDates;
                $scope.selected.startMonth = response.currentDate;
                $scope.selected.endMonth = response.currentDate;
                $scope.selected.currentMonth = response.currentDate;
                $scope.selected.summaryMonth = response.currentDate;
                
                $scope.selected.uid = $scope.currentUser.uid;
                $scope.selected.gid = $scope.currentGroup.groupid;
                $scope.availableCategories = response.availableCategories;
                
				$scope.trans.participants = response.participantsSelection;
                $scope.trans.date = new Date();
                $scope.loadSummary($scope.selected);
                $scope.retriveTrackingData($scope.selected);
            }
        });
    };
    
    $scope.autoload($scope.selected);

    $scope.loadSummary = function(selected) {
    	Data.post('loadSummary', {
    		selected: selected
        }).then(function(response) {
            Data.toast(response);
           
            $scope.summaryTable = response.summaryTable;
            $scope.nameTable = response.nameTable;
            $scope.summaryPaid = response.summaryPaid;
       
            $scope.message = response.message;
        });
    };

    $scope.retriveTrackingData = function(selected) {
        Data.post('retriveTrackingData', {
            selected: selected
        }).then(function(response) {
            Data.toast(response);
            $scope.selectTest = response.selectTest;
            if (response.status == "success") {
                $scope.selected.gid = response.selectedGID;
                $scope.selected.uid = response.selectedUID;
                $scope.selected.startMonth = response.selectedStartMonth;
                $scope.selected.endMonth = response.selectedEndMonth;;
                $scope.availableUsers = response.availableUsers;
                $scope.trans.participants = response.participantsSelection;
                $scope.currentGroup = response.currentGroup;
                $scope.trackingData = response.trackingData;
                $scope.message = response.message;
            } else {
                $scope.message = 'RetriveTracking Date Error';
            }
        });
    };

    $scope.startThisMonth = function() {
        $scope.selected.startMonth = new Date();
    };
    $scope.endThisMonth = function() {
        $scope.selected.endMonth = new Date();
    };
    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1,
        'datepicker-mode': "'month'",
        'min-mode': "month"
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }


    $scope.openStartMonth = function() {
        $scope.popupStartMonth.opened = true;
    };

    $scope.openEndMonth = function() {
        $scope.popupEndMonth.opened = true;
    };

    $scope.openSummaryMonth = function() {
        $scope.popupSummaryMonth.opened = true;
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popupStartMonth = {
        opened: false
    };

    $scope.popupEndMonth = {
        opened: false
    };

    $scope.popupSummaryMonth = {
            opened: false
        };
    $scope.addTransaction = function(selected, trans) {
        Data.post('addTransaction', {
        	selected: selected,
        	trans: trans,
        }).then(function(response) {
            Data.toast(response);
            $scope.message = response.message;
            $scope.loadSummary($scope.selected);
            $scope.retriveTrackingData($scope.selected);
            $scope.loadTest = response.loadTest;
        });
    };
    $scope.updateTransaction = function(hid, trans, creator) {
        Data.post('updateTransaction', {
            hid: hid,
            trans: trans,
            creator: creator
        }).then(function(response) {
            Data.toast(response);
            $scope.message = response.message;
            $scope.loadSummary($scope.selected);
            $scope.retriveTrackingData($scope.selected);
        });
    };
    $scope.removeTransaction = function(groupsOrEvents, hid) {
        Data.post('removeTransaction', {
            groupsOrEvents: groupsOrEvents,
        	hid: hid,
        }).then(function(response) {
            Data.toast(response);

            $scope.message = response.message;
            $scope.loadSummary($scope.selected);
            $scope.retriveTrackingData($scope.selected);

        });
    };



    $scope.toggleParticipantSelection = function(uid) {
        var idx = $scope.trans.participants.indexOf(uid);
        // is currently selected
        if (idx > -1) {
            $scope.trans.participants.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.trans.participants.push(uid);
        }
    };


    $scope.dtOptions = DTOptionsBuilder.newOptions().withDisplayLength(100).withPaginationType('full_numbers').withOption('rowCallback', rowCallback);
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7).notSortable()
    ];
    $scope.rowClickHandler = function(info) {
        $scope.trans.hid = info[0]
        /*$scope.trans.description = info[1];
        $scope.trans.amount = parseFloat(info[2]);
        $scope.trans.category = info[3];
        $scope.trans.date = new Date(info[4]);
        $scope.trans.participants = info[6];
        $scope.dataTablesInfo = 'hid:' + info[0] + 'des:'+info[1]
        +'amount:'+parseFloat(info[2])+'category:'+info[3]+'date'+info[4]+new Date(info[4])
        +'creator'+info[5]+'paticipants'+info[6]+'clicked ';
        */
        $scope.dataTablesInfo = 'hid:' + info[0] + ' clicked';
    }

    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function() {
            $scope.$apply(function() {
                $scope.rowClickHandler(aData);
            });
        });
        return nRow;
    };

    $scope.delecteConfirmation = function(groupsOrEvents, hid){
    	swal({   
    		  title: "Are you sure to delete transaction (HID="+ hid +")?",   
    		  text: "You will not be able to recover this transaction!",   
    		  type: "warning",   
    		  showCancelButton: true,   
    		  confirmButtonColor: "#DD6B55",   
    		  confirmButtonText: "Yes, delete it!",   
    		  closeOnConfirm: false
    		}, 
    		function(){        
    			$scope.removeTransaction(groupsOrEvents, hid);
    			swal("Deleted!", "Transaction (" + hid + ") has been deleted.", "success");  
    		});
    	}
    $rootScope.help = function(){
    	var html = "<h6>Monthly Summary Help</h6> " +
    		"<p style='text-align:left'>*Monthly Summary*<br>" +
    		"Pick up a month to view summary for specific month.<br>" +
    		"<br>" +
    		"The person in first column pays (owes) XX amount for person in first row.<br>" +
    		"For instance,<br>" +
    		"    x     y     z      Paid<br>" +
    		"x   20    5     -10    15<br>" +
    		"y   -5    20    -15    0<br>" +
    		"z	10    15    20     45<br>" +
    		"<br>" +
    		"x pays 20 dollars for x. (Meaning x spends 20 dollars in total for himself, this may be differenct from the amount in 'Paid')<br>" +
    		"x has paid 5 dollars for y.<br>" +
    		"x has owed 10 dollars from z.<br>" +
    		"x has paid 15 dollars in advance.<br>" +
    		"So, in this example x should give 10 to z, but get 5 from y.<br>" +
    		"In theory, sum of diagnal should equal to sum of column 'Paid'.<br>" +
    		"In the example, 20 + 20 + 20 = 15 + 0 + 45 <br>" +
    		"<br>" +
    		"*Monthly Details*<br>" +
    		"Start Month and End Month: choose start month and end month for spending data.<br>" +
    		"Group: choose from the group.<br>" +
    		"User/Disburser: Display the table on selected user's view AND set as 'Disburser' when add new record. <br>" +
    		"<b>IMPORTANT: In the table, 'Disburser' is the person who has paid the amount of money for all participants. <br>" +
    		"If select user x, the table will display at x's view, and when create new record, x will become the 'Disburser' automatically.</b><br>" +
    		"</p>";
    	swal({   
    		title: "<small>Spending Tracker 201</small>",   
    		 text: html,
    		 html: true 
    		});
    }

});