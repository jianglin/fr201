app.controller('testCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data) {
    //initially set those objects to null to avoid undefined error
    $scope.testDB = function () {
        Data.get('dbtest').then(function (reponse) {
            Data.toast(reponse);
            $scope.name = "TestDB";
            if (reponse.status == "success") {
            	 $scope.dbtests = reponse.dbtests;
            	 $scope.date = reponse.date;
            }
        });
    };
    $scope.testData = function () {
    	$http.get("http://www.w3schools.com/angular/customers_mysql.php")
        .then(function (response) {$scope.tests = response.data.records;});
    };
    $scope.logouttest = function () {
        Data.get('logouttest').then(function (results) {
            Data.toast(results);
            $location.path('login');
        });
    }
});