  <?php /*
    if (! isset ( $_SESSION )) {
   	session_start ();
   }
  echo '<pre>';
   var_dump ( $_SESSION );
   echo '</pre>'; */
   ?>


  <!-- 
  <p>Message: {{message}}</p>
   <p>Load test: {{loadTest}}. Select test: {{selectTest}}</p>
   <p>currentDate: {{currentDate}}. selectedStartMonth:
   	{{selected.startMonth | date:"yyyy-MM"}} . selectedEndMonth:
   	{{selected.endMonth | date:"yyyy-MM"}}</p>
   <p>currentGroup:{{currentGroup.groupid}}{{currentGroup.groupname}}.
   	selectedGroup: {{selected.gid}}</p>
   <p>currentUsr: {{currentUser.uid}}{{currentUser.name}}. selectedUser:
   	{{selected.uid}}.</p>
    <p>summary table: {{summaryTable}}</p>
   <p>total paid : {{summaryPaid}}</p>
 <p>name table: {{nameTable}}</p>
 -->
<div class="breadcrumbs" id="breadcrumbs">
   <ul class="breadcrumb">
      <li><i class="ace-icon fa fa-home home-icon"></i> <a href="#">Home</a>
      </li>
      <li class="active">Spending Tracker</li>
   </ul>
</div>
<div class="page-content">
<div class="row">
<div class="space-6"></div>
<div class="col-sm-10 col-sm-offset-1">
<div id="login-box" class="login-box visible widget-box no-border">
   <div class="widget-body">
      <div class="widget-main">
         <h4 class="header blue lighter bigger">
            <i class="icon-coffee green"></i> Spending Tracker
         </h4>
         <div class="space-16"></div>
         <p>UID: {{uid}} <br />NAME: {{name}} <br />E-MAIL: {{email}}</p>
		<br>
		<h4 ng-if="selected.groupsOrEvents=='groups'">
            Mothly Summary
         </h4>
         <h4 ng-if="selected.groupsOrEvents=='events'">
            Summary
         </h4>
		<form name="summaryForm" class="form-horizontal" role="form">
        <div class="form-group" ng-show="selected.groupsOrEvents=='groups'">
               <div class="col-sm-7 ">
                  <span> <input name="summary-month"
                     type="text" class="form-control date-picker"
                     uib-datepicker-popup ng-model="selected.summaryMonth"
                     ng-change="loadSummary(selected)"
                     is-open="popupSummaryMonth.opened" datepicker-popup="MM/yyyy"
                     datepicker-options="{minMode: 'month'}"
                     datepicker-mode="'month'" datepicker-options="dateOptions" />
                  <button type="button" class="btn btn-default"
                     ng-click="openSummaryMonth()">
                  <i class="glyphicon glyphicon-calendar"></i>
                  </button> <span> <em>{{selected.summaryMonth |
                  date:"yyyy-MM" }}</em></span>
                  </span>
               </div>
		</div>
		<table ng-table="summaryTable" class="table">
			<tr>
			<td></td>
			<td ng-repeat="name in nameTable">{{name}}</td>
			<td>Paid</td>
			</tr>
			<tr ng-repeat="(user_id,summary) in summaryTable" ng-style="user_id==selected.uid && {'background-color':'LightGreen'}">
				<td ng-repeat="(name_id, name) in nameTable" ng-if="user_id==name_id"><b>{{name}}</b></td>
				<td ng-repeat="(u_id, amount) in summary" ng-style="amount<0 && user_id==selected.uid && {'color':'Red'}">{{amount}}</td>
				<td ng-repeat="(t_id, total_amount) in summaryPaid" ng-if="t_id==user_id">{{total_amount}}</td>
				
			</tr>
		</table>
		<br>
		 <h4>
            Spending Details
         </h4>
		</form>
         <form name="selectForm" class="form-horizontal" role="form">
            <div class="form-group" ng-show="selected.groupsOrEvents=='groups'">
               <label class="col-sm-3 control-label no-padding-right" for="start-month">Start Month </label>
               <div class="col-sm-7">
                  <span> <input name="start-month"
                     type="text" class="form-control date-picker"
                     uib-datepicker-popup ng-model="selected.startMonth"
                     ng-change="retriveTrackingData(selected)"
                     is-open="popupStartMonth.opened" datepicker-popup="MM/yyyy"
                     datepicker-options="{minMode: 'month'}"
                     datepicker-mode="'month'" datepicker-options="dateOptions" />
                  <button type="button" class="btn btn-default"
                     ng-click="openStartMonth()">
                  <i class="glyphicon glyphicon-calendar"></i>
                  </button> <span>Start Month: <em>{{selected.startMonth |
                  date:"yyyy-MM" }}</em></span>
                  <button type="button" class="btn btn-sm btn-info" ng-click="startThisMonth(); retriveTrackingData(selected)">This
                  Month</button>
                  </span>
               </div>
            </div>
            <div class="form-group" ng-show="selected.groupsOrEvents=='groups'">
               <label class="col-sm-3 control-label no-padding-right" for="end-month">End Month </label>
               <div class="col-sm-7">
                  <span> <input name ="end-month"
                     type="text" class="form-control date-picker"
                     uib-datepicker-popup ng-model="selected.endMonth"
                     ng-change="retriveTrackingData(selected)"
                     is-open="popupEndMonth.opened" datepicker-popup="MM/yyyy"
                     datepicker-options="{minMode: 'month'}"
                     datepicker-mode="'month'" datepicker-options="dateOptions" />
                  <button type="button" class="btn btn-default"
                     ng-click="openEndMonth()">
                  <i class="glyphicon glyphicon-calendar"></i>
                  </button> <span>End Month: <em>{{selected.endMonth |
                  date:"yyyy-MM" }}</em></span>
                  <button type="button" class="btn btn-sm btn-info" ng-click="endThisMonth(); retriveTrackingData(selected)">This
                  Month</button>
                  </span>
               </div>
            </div>
            
            <div class="form-group">
            <div class="col-sm-5 control-label no-padding-right" >
            <span><label class="control-label no-padding-right" for="groups">Groups </label>
	         <input name="groups" type="radio" ng-model="selected.groupsOrEvents" value="groups" ng-change="autoload(selected)" />
            </span>
            <span>
            <label class="control-label no-padding-right" for="events">Events </label>
			<input name="events" type="radio" ng-model="selected.groupsOrEvents" value="events" ng-change="autoload(selected)" />
            </span>
            </div>
            </div>
            
            <div class="form-group">
               <label class="col-sm-3 control-label no-padding-right" for="availableGroups"> Group/Event </label>
               <div class="col-sm-7">
                  <select class="form-control" name="availableGroups" id="availableGroups" ng-model="selected.gid" ng-change="loadSummary(selected);retriveTrackingData(selected)">
                     <option ng-repeat="group in availableGroups"
                        ng-selected="{{group.gid == selected.gid}}"
                        value="{{group.gid}}">{{group.group_name}} ({{group.role}})</option>
                  </select>
               </div>
            </div>
            <div class="form-group">
               <label class="col-sm-3 control-label no-padding-right" for="availableUsers"> User/Disburser </label>
               <div class="col-sm-7">
                  <select class="form-control" name="availableUsers" id="availableUsers" ng-model="selected.uid" ng-change="retriveTrackingData(selected)">
                     <option ng-repeat="user in availableUsers"
                        ng-selected="{{user.uid == selected.uid}}"
                        value="{{user.uid}}">{{user.first_name}} ({{user.role}})</option>
                  </select>
               </div>
            </div>
         </form>
         <form name="transForm" role="form">
            <table datatable="ng" dt-options="dtOptions" dt-column-defs="dtColumnDefs" class="row-border hover">
               <thead>
                  <tr>
                     <th>
                     <th>
                        <div class="form-group">
                           <label>
                           <input class="trans-description" type="text" name="trans-description" id="trans-description" ng-model="trans.description" placeholder="Description" required>
                           </label>
                        </div>
                     </th>
                     <th>
                        <div class="form-group">
                           <label>
                           <input class="trans-amount" type="number" name="trans-amount" id="trans-amount" ng-model="trans.amount" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="1" placeholder="Amount" required>
                           </label>
                        </div>
                     </th>
                     <th>
                        <div class="form-group">
                           <label>
                              <select class="trans-category" name="trans-categories" id="trans-categories"
                                 ng-model="trans.category" required>
                                 <option ng-repeat="category in availableCategories" ng-selected="{{category.cid == trans.category_id}}"
                                    value="{{category.cid}}">{{category.category_name}}</option>
                              </select>
                           </label>
                        </div>
    
                     </th>
                     <th>
                    <div class="trans-date form-group">
                           <label>
                           <md-datepicker ng-model="trans.date" required></md-datepicker>
                           </label>
                        </div>
                        
                     <th>
                        <!-- disburser -->
                     <th>
                        <div class="form-group">
                           <div class="checkbox-lists" ng-model="trans.participants" ng-required="trans.participants">
                              <div ng-repeat="participant in availableUsers">
                                 <input class="trans-participants" id="participant-{{participant.uid}}" type="checkbox" ng-model="participant.isSelected" value="{{participant.name}}" ng-checked="trans.participants.indexOf(participant.uid) > -1" ng-click="toggleParticipantSelection(participant.uid)">
                                 <label for="participant-{{participant.uid}}"></label>{{participant.first_name}}
                              </div>
                              <span ng-if="!trans.participants.length" class="error-message">Check at least one user who participant in this transaction!</span>
                           </div>
                        </div>
                     </th>
                     <th>
                        <div class="form-group">
                           <button type="submit" class="btn btn-success" ng-click="addTransaction(selected, trans)" ng-disabled="transForm.$invalid || !trans.participants.length"><i class="fa fa-plus"></i></button>
                  
                        </div>
                     </th>
                  </tr>
                  <tr>
                     <th>HID</th>
                     <th>Description</th>
                     <th>Amount</th>
                     <th>Category</th>
                     <th>Date</th>
                     <th>Disburser</th>
                     <th>Participants</th>
                     <th></th>
                  </tr>
               </thead>
               <tbody>
                  <tr ng-repeat="transaction in trackingData" ng-style="transaction.has_participated && {'background-color':'LightGreen'}">
                     <td>{{ transaction.hid }}</td>
                     <td>{{ transaction.description }}</td>
                     <td>{{ transaction.amount }}</td>
                     <td>{{ transaction.category_name }}</td>
                     <td>{{ transaction.transaction_time }}</td>
                     <td>{{ transaction.first_name }}</td>
                     <td>{{ transaction.user_lists }}</td>
                     <td>
                        <!-- <button type="button" class="btn btn-warning" ng-model="transaction.hid" ng-click="updateTransaction(transaction.hid, trans, selected.uid)" ng-disabled="transForm.$invalid || !trans.participants.length"><i class="fa fa-edit"></i></button> -->
                        <button type="button" class="btn btn-danger" ng-model="transaction.hid" ng-click="delecteConfirmation(selected.groupsOrEvents,transaction.hid)"><i class="fa fa-trash-o"></i></button>
                     </td>
                  </tr>
               </tbody>
            </table>
         </form>
         <p>INFO:{{dataTablesInfo}}</p>
      </div>
   </div>
   </div>
   </div>
   </div>
</div>
<!-- /.page-content -->