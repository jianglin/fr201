<?php
echo "this is php page";   
if (!isset($_SESSION)) {
	session_start();
}
echo '<pre>';
var_dump($_SESSION);
echo '</pre>';
?>

<div class="breadcrumbs" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="#">Home</a>
        </li>
        <li class="active">Test</li>
    </ul>
</div>
<div class="page-content">
    <div class="row">
        <div class="space-6"></div>
        <div class="col-sm-10 col-sm-offset-1">
            <div id="login-box" class="login-box visible widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header blue lighter bigger">
                    <i class="icon-coffee green"></i>
                    User Authenticated
                </h4>
                        <div class="space-16"></div>
                        UID: {{uid}}
                        <br/>NAME: {{name}}
                        <br/>E-MAIL: {{email}}
						<br/>TEST: <table>
									  <tr ng-repeat="x in tests">
										<td>{{ x.Name }}</td>
										<td>{{ x.Country }}</td>
									  </tr>
									</table>
                        <br/>
						<br/>DBTEST: {{dbtests}}</td>
									<table>
									  <tr ng-repeat="x in dbtests">
										<td>{{ x.Name }}</td>
										<td>{{ x.City }}</td>
										<td>{{ x.Country }}</td>
									  </tr>
									</table>
						<br/>Date from database: {{date}}			
						<br/>Message: {{message}}
						<br/>
                        <?php echo "php session info: " . $_SESSION['name'];?>
                        <br/>
						<a ng-click="testData();">testData</a>
                        <br/>
						<a ng-click="testDB();">testDB</a> 
						<br/>
						<a ng-click="logouttest();">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.page-content -->